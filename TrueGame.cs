﻿using GameTools;
using System;
using System.Collections.Generic;

namespace MyFirstProgram
{
    internal class TrueGame : GameEngine
    {
        MatrixChanged matrix;
        int ownLetters;
        int position;
        bool space = false;
        //base.BackgroundConsoleColor

        public TrueGame()
        {
            //base.BackgroundConsoleColor = ConsoleColor.Magenta;
            /*Start();
            Update();
            Exit();*/
        }

        public override void Start()
        {
            //base.InitGame();
            //Console.BackgroundColor = base.BackgroundConsoleColor;
            ownLetters = 0;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Presiona las flechas izquierda y derecha para mover la flecha de la parte superior (▼)\nPulsa espacio para lanzar una letra aleatoria donde se encuentre la flecha");
            //Console.ForegroundColor= ConsoleColor.White;
            matrix = new MatrixChanged(25, 25);
            position = 0;
            matrix.CleanTheMatrix();

            for (var i = 0; i < matrix.TheMatrix.GetLength(0); i++)
            {
                matrix.TheMatrix[0, i] = ' ';
            }
        }

        public override void Update()
        {
            /*while (true)
            {*/
            matrix.TheMatrix[0, position] = '▼';
            FallingLetters();
            LetterGeneratorInMatrix();
            if (space)
            {
                space = false;
                LetterGeneratorInMatrix(position);
            }
            matrix.PrintMatrix();
            //System.Threading.Thread.Sleep(time2liveframe);
            //}
        }

        public void LetterGeneratorInMatrix()
        {
            var rnd = new Random();
            var done = false;

            do
            {
                var ranNum = rnd.Next(0, matrix.TheMatrix.GetLength(0));
                if (matrix.TheMatrix[1, ranNum] == '0')
                {
                    matrix.TheMatrix[1, ranNum] = LetterGenerator(rnd);
                    done = true;
                }
                    
            } while (!done);
            
        }

        public void LetterGeneratorInMatrix(int position)
        {
            var rnd = new Random();
            matrix.TheMatrix[1, position] = LetterGenerator(rnd);
        }

        public char LetterGenerator(Random rnd)
        {
            char c;
            do
            {
                c = (char)rnd.Next(65, 90);
            } while (c == '0');
           
            return c;
        }

        public void FallingLetters()
        {
            
            var positionList = new List<int[]>();
            for (var i = matrix.TheMatrix.GetLength(0) - 1; i >= 0; i--)
            {
                for (var j = matrix.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                {
                    if (matrix.TheMatrix[i, j] != '0' && matrix.TheMatrix[i, j] != ' ' && matrix.TheMatrix[i, j] != '▼')
                    {
                        //(matrix.TheMatrix[i, j], matrix.TheMatrix[i + 1, j]) = ('0', 'X');
                        positionList.Add(new int[] { i, j });
                    }
                }
            }

            foreach (var p in positionList)
            {
                if (p[0] + 1 < matrix.TheMatrix.GetLength(0))
                {
                    (matrix.TheMatrix[p[0], p[1]], matrix.TheMatrix[p[0] + 1, p[1]]) = ('0', matrix.TheMatrix[p[0], p[1]]);
                }
                
                else matrix.TheMatrix[p[0], p[1]] = '0';
            }
        }

        /*public void MatrixShow()
        {
            for (var i = 0; i < matrix.TheMatrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.TheMatrix.GetLength(1); j++)
                {
                    var c = matrix.TheMatrix[i, j];
                    if (c != '0') Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write(c);
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.WriteLine();
            }
        }*/

        protected override void UpdateGame()
        {
            do
            {
                while (Console.KeyAvailable == false)
                {
                    CleanFrame();
                    Update();
                    RefreshFrame();
                    Frames++;
                }
                cki = Console.ReadKey(true);
                CheckKeyboard4Engine();
            } while (!engineSwitch);
        }

        protected override void CheckKeyboard4Engine()
        {
            base.CheckKeyboard4Engine();
            if (!engineSwitch)
            {
                if (cki.Key == ConsoleKey.LeftArrow && position > 0)
                {
                    matrix.TheMatrix[0, position] = ' ';
                    position--;
                }
                else if (cki.Key == ConsoleKey.RightArrow && position < matrix.TheMatrix.GetLength(0) - 1)
                {
                    matrix.TheMatrix[0, position] = ' ';
                    position++;
                }
                else if (cki.Key == ConsoleKey.Spacebar)
                {
                    ownLetters++;
                    space = true;
                }
            }
        }

        public override void Exit()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Total frames: " + Frames + ". Total letters manually created: " + ownLetters);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
