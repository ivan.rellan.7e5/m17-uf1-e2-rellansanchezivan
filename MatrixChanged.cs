﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstProgram
{
    public class MatrixChanged : MatrixRepresentation
    {
        public MatrixChanged(int sizeX, int sizeY) : base(sizeX, sizeY)
        {}

        public override void PrintMatrix()
        {
            for (var i = 0; i < TheMatrix.GetLength(0); i++)
            {
                for (var j = 0; j < TheMatrix.GetLength(1); j++)
                {
                    var c = TheMatrix[i, j];
                    if (c == '▼') Console.ForegroundColor = ConsoleColor.Magenta;
                    else if (c != '0') Console.ForegroundColor = ConsoleColor.Blue;

                    Console.Write(" " + c);
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.WriteLine();
            }
        }
    }
}
