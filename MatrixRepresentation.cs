﻿using System;
namespace MyFirstProgram
{
    public class MatrixRepresentation
    {
        //Matriz de caracteres a mostrar en consola
        private char[,] _theMatrix;
        public char[,] TheMatrix
        {
            set
            {
                // _theMatrix = value;
                _theMatrix = ClippingMatrix(value);
            }
            get
            {
                return _theMatrix;
            }
        }


        private int sizeX, sizeY;

        public MatrixRepresentation()
        {
            InitMatrix(null, null);
        }

        public MatrixRepresentation(int width, int height)
        {
            InitMatrix(width, height);
        }

        //Asigna valores al tamaño de la matriz si estos no son null
        private void InitMatrix(int? _sizeX, int? _sizeY)
        {
            //cambio aleatorio
            sizeX = (_sizeX.HasValue) ? _sizeX.Value : 9;
            sizeY = (_sizeY.HasValue) ? _sizeY.Value : 9;
            _theMatrix = new char[sizeX, sizeY];
        }

        //Limpia la matriz
        public void CleanMatrix()
        {
            InitMatrix(sizeX, sizeY);
        }

        //Muestra la matriz
        public virtual void PrintMatrix()
        {
            Console.Clear();
            for (int x = 0; x < _theMatrix.GetLength(0); x++)
            {
                for (int y = 0; y < _theMatrix.GetLength(1); y++)
                {
                    Console.Write($" {_theMatrix[x, y]}");
                }
                Console.WriteLine();
            }
        }

        //Vaciar la matriz actual
        public void CleanTheMatrix()
        {
            _theMatrix = BlankMatrix();
        }

        //Crea una matriz vacía
        public char[,] BlankMatrix()
        {
            char[,] tm = new char[_theMatrix.GetLength(0), _theMatrix.GetLength(1)];

            for (int x = 0; x < tm.GetLength(0); x++)
            {
                for (int y = 0; y < tm.GetLength(1); y++)
                {
                    tm[x, y] = '0';
                }
            }
            return tm;
        }

        //Copiar una matriz
        public char[,] ClippingMatrix(char[,] toClipper)
        {
            int limitX = (toClipper.GetLength(0) > _theMatrix.GetLength(0)) ? _theMatrix.GetLength(0) : toClipper.GetLength(0);
            int limitY = (toClipper.GetLength(1) > _theMatrix.GetLength(1)) ? _theMatrix.GetLength(1) : toClipper.GetLength(1);

            for (int x = 0; x < limitX; x++)
            {
                for (int y = 0; y < limitY; y++)
                {
                    _theMatrix[x, y] = toClipper[x, y];
                }
            }   
            return _theMatrix;
        }
    }
}

